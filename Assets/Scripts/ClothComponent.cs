﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubeAndCloth {
	public class ClothComponent : MonoBehaviour {
		[SerializeField, Range(3, 5)]
		private int _minLineLength = 3;
		[SerializeField, Range(5, 7)]
		private int _maxLineLength = 7;
		[SerializeField, Range(1, 5)]
		private int _distance = 2;

		private int[] _polyline;

		public int GetRandomLength => Random.Range(_minLineLength, _maxLineLength + 1);
		public int[] GetPolyline => _polyline;
		public int GetDistance => _distance;

		private void Awake() {
			_polyline = new int[GetComponentInChildren<GridGenerator>().GetYSize + 1];

			for(int i = 0; i < _polyline.Length; i++) {
				_polyline[i] = GetRandomLength;
			}

			_polyline[0] = 5;
		}
	}
}