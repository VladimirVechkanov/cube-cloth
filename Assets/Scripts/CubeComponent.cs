﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CubeAndCloth {
	public class CubeComponent : MonoBehaviour {
		[SerializeField, Range(0.1f, 10f)]
		private float _moveSpeed = 0.1f;
		[SerializeField]
		private float _minDistanceForRotate = 1f, _yPosForFinish = 0.6f, _multiplier = 5f;

		private void Start() {
			//StartCoroutine(Rotator());
		}

		private void Update() {
			if(transform.position.y >= _yPosForFinish) {
				SceneManager.LoadScene("GameScene");
			}
		}

		private void FixedUpdate() {
			transform.position += transform.up * _moveSpeed * Time.fixedDeltaTime;
		}

		private IEnumerator Rotator() { // сначала неправильно понял задание, сделал этот метод, чтобы куб сам поворачивал на поворотах)
			while(true) {
				var posL = transform.position - transform.right / 5f + transform.up / 3.5f;
				var posR = transform.position + transform.right / 5f + transform.up / 3.5f;

				var hitL = Physics2D.Raycast(new Vector2(posL.x, posL.y), new Vector2(transform.up.x, transform.up.y), _minDistanceForRotate);
				var hitR = Physics2D.Raycast(new Vector2(posR.x, posR.y), new Vector2(transform.up.x, transform.up.y), _minDistanceForRotate);

#if UNITY_EDITOR
				Debug.DrawRay(posL, new Vector2(transform.up.x, transform.up.y), Color.red);
				Debug.DrawRay(posR, new Vector2(transform.up.x, transform.up.y), Color.red);
#endif

				if(hitL.transform != null && hitR.transform != null && hitL.transform != transform && hitR.transform != transform) {
					if(hitL.distance < hitR.distance) {
						Rotate(RotateDir.Right);
					}
					else if(hitL.distance > hitR.distance) {
						Rotate(RotateDir.Left);
					}
				}

				yield return null;
			}
		}

		private void Rotate(RotateDir rotateDir) {
			if(rotateDir == RotateDir.Right) {
				transform.rotation *= Quaternion.Euler(0f, 0f, -1f);
			}
			else {
				transform.rotation *= Quaternion.Euler(0f, 0f, 1f);
			}
		}
	}

	public enum RotateDir : byte {
		Left,
		Right
	}
}