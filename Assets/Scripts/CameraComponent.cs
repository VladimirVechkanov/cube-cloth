﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CubeAndCloth {
	public class CameraComponent : MonoBehaviour {
		[SerializeField]
		private Transform _cube;

		private void Update() {
			transform.position = new Vector3(transform.position.x, _cube.position.y, transform.position.z);
		}
	}
}