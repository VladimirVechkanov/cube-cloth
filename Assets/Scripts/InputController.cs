﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CubeAndCloth {
	public class InputController : MonoBehaviour, IBeginDragHandler, IDragHandler {
		[SerializeField]
		private Transform _cube;
		[SerializeField]
		private float _offset;

		public void OnBeginDrag(PointerEventData eventData) {
			if((Mathf.Abs(eventData.delta.x)) > (Mathf.Abs(eventData.delta.y))) {
				if(eventData.delta.x > 0) {
					StartCoroutine(MoveCube(-_offset));
				}
				else {
					StartCoroutine(MoveCube(_offset));
				}
			}
		}

		private IEnumerator MoveCube(float targetZ) {
			var time = 0.1f;

			while(time > 0f) {
				_cube.rotation *= Quaternion.Euler(0f, 0f, targetZ);
				time -= Time.deltaTime;

				yield return null;
			}
		}

		public void OnDrag(PointerEventData eventData) {
			
		}
	}
}