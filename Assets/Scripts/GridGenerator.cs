﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace CubeAndCloth {
	[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
	public class GridGenerator : MonoBehaviour {
		[SerializeField]
		private bool _grid2;

		private int _xSize = 4, _ySize = 6;
		private Vector3[] _vertices;
		private Mesh _mesh;
		private ClothComponent _clothComponent;
		private List<Vector2> _points;

		public int GetYSize => _ySize;

		private void Start() {
			_clothComponent = GetComponentInParent<ClothComponent>();

			if(!_grid2) {
				transform.position -= new Vector3((float)_clothComponent.GetDistance / 2, 0f, 0f);
			}
			else {
				transform.position += new Vector3((float)_clothComponent.GetDistance / 2, 0f, 0f);
			}

			Generate();
		}

		private void Generate() {
			_mesh = new Mesh();
			GetComponent<MeshFilter>().mesh = _mesh;
			_mesh.name = "Grid";

			_points = new List<Vector2>();

			var polyline = GeneratePolyline();

			_vertices = new Vector3[(_xSize + 1) * (_ySize + 1)];
			Vector2[] uvs = new Vector2[_vertices.Length];
			Vector4[] tangents = new Vector4[_vertices.Length];
			Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);
			_points.Add(new Vector2(0, _ySize));
			_points.Add(new Vector2(0, 0));
			for(int i = 0, y = 0; y <= _ySize; y++) {
				for(int x = 0; x <= _xSize; x++, i++) {
					if(!_grid2 && x == _xSize) {
						_vertices[i] = new Vector3(polyline[y], y);
					}
					else if(_grid2 && x == _xSize) {
						_vertices[i] = new Vector3(polyline[y], y);
					}
					else {
						_vertices[i] = new Vector3(x, y);
					}

					if(x == _xSize) {
						_points.Add(new Vector2(_vertices[i].x, _vertices[i].y));
					}

					uvs[i] = new Vector2((float)x / _xSize, (float)y / _ySize);
					tangents[i] = tangent;
				}
			}
			_mesh.vertices = _vertices;
			_mesh.uv = uvs;
			_mesh.tangents = tangents;

			GetComponent<PolygonCollider2D>().points = _points.ToArray();

			int[] triangles = new int[_xSize * _ySize * 6];
			int ti = 0, vi = 0;
			for(int y = 0; y < _ySize; y++, vi++) {
				for(int x = 0; x < _xSize; x++, ti += 6, vi++) {
					triangles[ti] = vi;
					triangles[ti + 1] = vi + _xSize + 1;
					triangles[ti + 2] = vi + 1;
					triangles[ti + 3] = vi + 1;
					triangles[ti + 4] = vi + _xSize + 1;
					triangles[ti + 5] = vi + _xSize + 2;
				}
			}

			_mesh.triangles = triangles;
			_mesh.RecalculateNormals();
		}

		private int[] GeneratePolyline() {
			var polyline = _clothComponent.GetPolyline;

			if(_grid2) {
				var polyline2 = new int[_ySize + 1];

				for(int i = 0, j = 6; i < polyline2.Length; i++, j--) {
					polyline2[i] = _xSize + _ySize - polyline[j];
				}

				return polyline2;
			}

			return polyline;
		}

		private Vector2[] ConvertVector3ToVector2(Vector3[] vector3) {
			var vector2 = new Vector2[vector3.Length];

			for(int i = 0; i < vector2.Length; i++) {
				vector2[i] = new Vector2(vector3[i].x, vector3[i].y);
			}

			return vector2;
		}
	}
}